package kafkaerr

import "fmt"

type tDoNotRetry struct{ err error }

func (e tDoNotRetry) Error() string { return fmt.Sprintf("Failed: %v", e.err) }

func DoNotRetry(err error) error { return tDoNotRetry{err} }

func IsDoNotRetry(err error) bool {
	_, ok := err.(tDoNotRetry)
	return ok
}
