package committer

import (
	"log"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

type Committer struct {
	consumer                 *kafka.Consumer
	commitEveryMessagesCount uint
	commitAfterFirstMessage  time.Duration
	beforeCommit             func() error

	messagesCount uint
	commitAt      time.Time
	doNotCommit   bool
}

func New(
	consumer *kafka.Consumer,
	commitEveryMessagesCount uint,
	commitAfterFirstMessage time.Duration,
	beforeCommit func() error,
) *Committer {
	return &Committer{
		consumer:                 consumer,
		commitEveryMessagesCount: commitEveryMessagesCount,
		commitAfterFirstMessage:  commitAfterFirstMessage,
		beforeCommit:             beforeCommit,
	}
}

func (co *Committer) DoneProcessMessage() {
	if co.messagesCount == 0 {
		co.commitAt = time.Now().Add(co.commitAfterFirstMessage)
	}
	co.messagesCount++
}

func (co *Committer) DoNotCommit() { co.doNotCommit = true }

func (co *Committer) CheckIfCanCommit(commitNow bool) error {
	if co.doNotCommit || co.messagesCount == 0 {
		return nil
	}
	if commitNow || co.messagesCount >= co.commitEveryMessagesCount || time.Now().After(co.commitAt) {
		if co.beforeCommit != nil {
			err := co.beforeCommit()
			if err != nil {
				log.Println("error running beforeCommit():", err)
				return err
			}
		}
		_, err := co.consumer.Commit()
		if err != nil {
			return err
		}
		co.messagesCount = 0
	}
	return nil
}
