package kafkaconsumer

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/stefarf/iferr"

	"gitlab.com/stefarf/kafkaconsumer/internal/committer"
)

type Signal interface {
	Terminated() bool
}

type Options struct {
	// CommitAfterFirstMessage default value is 1 second
	CommitAfterFirstMessage time.Duration

	// CommitEveryMessagesCount default value is 100
	CommitEveryMessagesCount uint

	// NumberOfPartitions default value is 10
	NumberOfPartitions uint

	BeforeCommit func() error
}

func SubscribeAndConsume(
	bootstrapServers, topics []string,
	groupId string,
	signal Signal,
	consumeFunc func(m *kafka.Message) (commitNow bool, err error),
	opts Options,
) error {
	if opts.CommitAfterFirstMessage == 0 {
		opts.CommitAfterFirstMessage = time.Second
	}
	if opts.CommitEveryMessagesCount == 0 {
		opts.CommitEveryMessagesCount = 100
	}
	if opts.NumberOfPartitions == 0 {
		opts.NumberOfPartitions = 10
	}

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":  strings.Join(bootstrapServers, ","),
		"group.id":           groupId,
		"auto.offset.reset":  "earliest",
		"enable.auto.commit": "false",
	})
	if err != nil {
		return err
	}
	defer func() { iferr.Print(consumer.Close()) }()

	log.Println("Consumer created")

	err = consumer.SubscribeTopics(topics, nil)
	if err != nil {
		return err
	}

	log.Printf("Subscribed to topics %v, consumer is READY", topics)

	com := committer.New(consumer, opts.CommitEveryMessagesCount, opts.CommitAfterFirstMessage, opts.BeforeCommit)
	defer func() { iferr.Print(com.CheckIfCanCommit(true)) }()
	for {
		if signal.Terminated() {
			return nil
		}
		ev := consumer.Poll(100)
		switch e := ev.(type) {
		case *kafka.Message:
			commitNow, err := consumeFunc(e)
			if err != nil {
				log.Println("error running consumeFunc():", err)
				com.DoNotCommit()
				return err
			}
			com.DoneProcessMessage()
			err = com.CheckIfCanCommit(commitNow)
			if err != nil {
				com.DoNotCommit()
				return err
			}
		case kafka.Error:
			com.DoNotCommit()
			return e
		default:
			err := com.CheckIfCanCommit(false)
			if err != nil {
				com.DoNotCommit()
				return err
			}
			if e != nil {
				log.Println("event:", e)
			}
		}
	}
}

func CreateTopics(bootstrapServers, topics []string, numberOfPartitions int) error {
	adm, err := kafka.NewAdminClient(&kafka.ConfigMap{
		"bootstrap.servers": strings.Join(bootstrapServers, ","),
	})
	if err != nil {
		return err
	}
	var topicSpecs []kafka.TopicSpecification
	for _, topic := range topics {
		topicSpecs = append(topicSpecs, kafka.TopicSpecification{
			Topic:         topic,
			NumPartitions: numberOfPartitions,
		})
	}
	_, err = adm.CreateTopics(context.Background(), topicSpecs, kafka.SetAdminOperationTimeout(time.Minute))
	if err != nil {
		log.Println("Error create topics:", err)
		return err
	}
	log.Println("Topics created:", topics)
	return nil
}
