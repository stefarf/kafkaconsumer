#!/usr/bin/env bash
set -e
go test -coverprofile=cover.out -v
go tool cover -html=cover.out
rm cover.out
