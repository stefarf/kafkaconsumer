package consumeandretry

import "fmt"

func errTopicHasNoConsumer(topic string) error {
	return fmt.Errorf("topic '%s' has no consumer assigned", topic)
}
