package consumeandretry_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/stretchr/testify/require"

	"gitlab.com/stefarf/kafkaconsumer/consumeandretry"
	"gitlab.com/stefarf/kafkaconsumer/consumeandretry/internal/testhelper"
	"gitlab.com/stefarf/kafkaconsumer/consumermap"
	"gitlab.com/stefarf/kafkaconsumer/kafkaerr"
)

const (
	anyError = "any error"

	registeredTopic       = "registered.topic"
	registeredTopicRetry  = registeredTopic + ".retry"
	registeredTopicFailed = registeredTopic + ".failed"

	unknownTopic       = "unknown.topic"
	unknownTopicFailed = unknownTopic + ".failed"

	retryDelay    = time.Second * 3
	retryDuration = time.Second * 8
)

var (
	anyKey   = []byte("key")
	anyValue = []byte("value")

	now       = time.Date(2024, 1, 1, 1, 1, 1, 1, time.Local)
	runAfter  = now.Add(retryDelay)
	runBefore = now.Add(retryDuration)
)

func TestUsecase_TestConsumeAndRetry(t *testing.T) {
	t.Run("panic if", func(t *testing.T) {
		runTest := func(title, expectedError string, cb func(opts consumeandretry.Options) consumeandretry.Options) {
			t.Run(fmt.Sprintf("panic if %s", title), func(t *testing.T) {
				defer func() {
					if r := recover(); r != nil {
						if r.(string) != expectedError {
							t.Error("unexpected error message", r)
						}
					}
				}()
				opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
				opts = cb(opts)
				opts.Fn(nil)
				t.Error("should panic")
			})
		}

		runTest("RetryDelay = 0", "empty RetryDelay or empty RetryDuration",
			func(opts consumeandretry.Options) consumeandretry.Options {
				opts.RetryDelay = 0
				return opts
			})
		runTest("RetryDuration = 0", "empty RetryDelay or empty RetryDuration",
			func(opts consumeandretry.Options) consumeandretry.Options {
				opts.RetryDuration = 0
				return opts
			})
		runTest("produce function is nil", "empty Produce func",
			func(opts consumeandretry.Options) consumeandretry.Options {
				opts.Produce = nil
				return opts
			})
	})

	t.Run("do not consume failed message (message sent to topic with '.failed' suffix)", func(t *testing.T) {
		opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
		f := opts.Fn(nil)
		commitNow, err := f(&kafka.Message{
			TopicPartition: kafka.TopicPartition{
				Topic: testhelper.PtrString(registeredTopicFailed),
			},
		})
		require.Equal(t, nil, err)
		require.False(t, commitNow)
	})

	t.Run("produce failed message if topic not registered in the consumer map", func(t *testing.T) {
		expectedHeaders := []kafka.Header{
			{
				Key:   consumeandretry.HeaderError,
				Value: []byte(fmt.Sprintf("topic '%s' has no consumer assigned", unknownTopic)),
			},
		}

		opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
		opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
			unknownTopicFailed, string(anyKey), anyValue, expectedHeaders,
			nil)
		f := opts.Fn(consumermap.New().RegisterWithRetry(
			registeredTopic,
			func(m *kafka.Message) error { panic("unexpected") },
		))
		commitNow, err := f(&kafka.Message{
			TopicPartition: kafka.TopicPartition{
				Topic: testhelper.PtrString(unknownTopic),
			},
			Key:   anyKey,
			Value: anyValue,
		})
		require.Equal(t, nil, err)
		require.False(t, commitNow)
	})

	t.Run("consume message in registered topic", func(t *testing.T) {
		t.Run("if consumer return nil (no error), then do not need to retry", func(t *testing.T) {
			runConsumerFunction := false

			opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
			f := opts.Fn(consumermap.New().RegisterWithRetry(
				registeredTopic,
				func(m *kafka.Message) error {
					runConsumerFunction = true
					return nil
				},
			))
			commitNow, err := f(&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic: testhelper.PtrString(registeredTopic),
				},
			})

			require.Equal(t, nil, err)
			require.False(t, commitNow)
			require.True(t, runConsumerFunction)
		})

		t.Run("if consumer return do-not-retry-error, then produce retry message with error message, error count, run after and run before headers",
			func(t *testing.T) {
				opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
				opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
					registeredTopicFailed, string(anyKey), anyValue, []kafka.Header{
						{Key: consumeandretry.HeaderError, Value: []byte("Failed: " + anyError)}, // error message from kafkaerr.DoNotRetry
						{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
					},
					nil)
				f := opts.Fn(consumermap.New().RegisterWithRetry(
					registeredTopic,
					func(m *kafka.Message) error { return kafkaerr.DoNotRetry(errors.New(anyError)) }, // consumer returns do not retry error
				))
				commitNow, err := f(&kafka.Message{
					TopicPartition: kafka.TopicPartition{
						Topic: testhelper.PtrString(registeredTopic),
					},
					Key:   anyKey,
					Value: anyValue,
				})

				require.Equal(t, nil, err)
				require.False(t, commitNow)
			})

		t.Run("if consumer return other error, then produce retry message with error message, error count, run after and run before headers",
			func(t *testing.T) {
				opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
				opts.Now = func() time.Time { return now }
				opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
					registeredTopicRetry, string(anyKey), anyValue, []kafka.Header{
						{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
						{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
						{Key: consumeandretry.HeaderRunAfter, Value: []byte(runAfter.Format(time.RFC3339))},
						{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
					},
					nil)
				f := opts.Fn(consumermap.New().RegisterWithRetry(
					registeredTopic,
					func(m *kafka.Message) error { return errors.New(anyError) }, // consumer returns error
				))
				commitNow, err := f(&kafka.Message{
					TopicPartition: kafka.TopicPartition{
						Topic: testhelper.PtrString(registeredTopic),
					},
					Key:   anyKey,
					Value: anyValue,
				})

				require.Equal(t, nil, err)
				require.False(t, commitNow)
			})
	})

	t.Run("consume message in registered topic with .retry suffix", func(t *testing.T) {
		t.Run("sleep if now before run after header and then run consumer function", func(t *testing.T) {
			t.Run("case 1: if consumer function return nil (no error), then done", func(t *testing.T) {
				runConsumerFunction := false

				opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
				opts.Now = func() time.Time { return now }
				opts.Sleep = testhelper.MustSleepSecs(t, retryDelay.Seconds())
				f := opts.Fn(consumermap.New().RegisterWithRetry(
					registeredTopicRetry,
					func(m *kafka.Message) error {
						runConsumerFunction = true
						return nil
					},
				))
				commitNow, err := f(&kafka.Message{
					TopicPartition: kafka.TopicPartition{
						Topic: testhelper.PtrString(registeredTopicRetry),
					},
					Key:   anyKey,
					Value: anyValue,
					Headers: []kafka.Header{
						{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
						{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
						{Key: consumeandretry.HeaderRunAfter, Value: []byte(runAfter.Format(time.RFC3339))},
						{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
					},
				})

				require.Equal(t, nil, err)
				require.False(t, commitNow)
				require.True(t, runConsumerFunction)
			})

			t.Run("case 2: if consumer function return error and the 'run after' < 'run before', then produce retry message",
				func(t *testing.T) {
					runConsumerFunction := false

					nowLst := []time.Time{now, runAfter, runAfter.Add(time.Second)}
					nextRunAfter := nowLst[len(nowLst)-1].Add(retryDelay)

					opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
					opts.Now = func() time.Time {
						n := nowLst[0]
						nowLst = nowLst[1:]
						return n
					}
					opts.Sleep = testhelper.MustSleepSecs(t, retryDelay.Seconds())
					opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
						registeredTopicRetry, string(anyKey), anyValue, []kafka.Header{
							{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
							{Key: consumeandretry.HeaderErrorCount, Value: []byte("2")},
							{Key: consumeandretry.HeaderRunAfter, Value: []byte(nextRunAfter.Format(time.RFC3339))},
							{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
						},
						nil)
					f := opts.Fn(consumermap.New().RegisterWithRetry(
						registeredTopicRetry,
						func(m *kafka.Message) error {
							runConsumerFunction = true
							return errors.New(anyError)
						},
					))
					commitNow, err := f(&kafka.Message{
						TopicPartition: kafka.TopicPartition{
							Topic: testhelper.PtrString(registeredTopicRetry),
						},
						Key:   anyKey,
						Value: anyValue,
						Headers: []kafka.Header{
							{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
							{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
							{Key: consumeandretry.HeaderRunAfter, Value: []byte(runAfter.Format(time.RFC3339))},
							{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
						},
					})

					require.Equal(t, nil, err)
					require.False(t, commitNow)
					require.True(t, runConsumerFunction)
				})

			t.Run("case 3: if consumer function return error and the 'run after' > 'run before', then produce failed message",
				func(t *testing.T) {
					runConsumerFunction := false

					nowLst := []time.Time{now, runAfter, runAfter.Add(time.Second * 3)}

					opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
					opts.Now = func() time.Time {
						n := nowLst[0]
						nowLst = nowLst[1:]
						return n
					}
					opts.Sleep = testhelper.MustSleepSecs(t, retryDelay.Seconds())
					opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
						registeredTopicFailed, string(anyKey), anyValue, []kafka.Header{
							{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
							{Key: consumeandretry.HeaderErrorCount, Value: []byte("2")},
						},
						nil)
					f := opts.Fn(consumermap.New().RegisterWithRetry(
						registeredTopicRetry,
						func(m *kafka.Message) error {
							runConsumerFunction = true
							return errors.New(anyError)
						},
					))
					commitNow, err := f(&kafka.Message{
						TopicPartition: kafka.TopicPartition{
							Topic: testhelper.PtrString(registeredTopicRetry),
						},
						Key:   anyKey,
						Value: anyValue,
						Headers: []kafka.Header{
							{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
							{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
							{Key: consumeandretry.HeaderRunAfter, Value: []byte(runAfter.Format(time.RFC3339))},
							{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
						},
					})

					require.Equal(t, nil, err)
					require.False(t, commitNow)
					require.True(t, runConsumerFunction)
				})
		})

		t.Run("produce failed message if now is after the 'run before' header", func(t *testing.T) {
			opts := testhelper.ConsumeRetryDefaultOptions(retryDelay, retryDuration)
			opts.Now = func() time.Time { return runBefore.Add(time.Second) }
			opts.Produce = testhelper.ProduceFnMustBeCalledWithArgsAndReturn(
				registeredTopicFailed, string(anyKey), anyValue, []kafka.Header{
					{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
					{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
				},
				nil)
			f := opts.Fn(consumermap.New().RegisterWithRetry(
				registeredTopicRetry,
				func(m *kafka.Message) error { panic("unexpected") },
			))
			commitNow, err := f(&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic: testhelper.PtrString(registeredTopicRetry),
				},
				Key:   anyKey,
				Value: anyValue,
				Headers: []kafka.Header{
					{Key: consumeandretry.HeaderError, Value: []byte(anyError)},
					{Key: consumeandretry.HeaderErrorCount, Value: []byte("1")},
					{Key: consumeandretry.HeaderRunAfter, Value: []byte(runAfter.Format(time.RFC3339))},
					{Key: consumeandretry.HeaderRunBefore, Value: []byte(runBefore.Format(time.RFC3339))},
				},
			})

			require.Equal(t, nil, err)
			require.False(t, commitNow)
		})
	})
}
