package consumeandretry

import (
	"errors"
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"

	"gitlab.com/stefarf/kafkaconsumer/consumermap"
	"gitlab.com/stefarf/kafkaconsumer/kafkaerr"
)

const (
	extRetry  = ".retry"
	extFailed = ".failed"
)

var zeroTime = time.Time{}

type Options struct {
	RetryDelay    time.Duration
	RetryDuration time.Duration

	Produce func(topic, key string, value any, headers []kafka.Header) error
	Now     func() time.Time
	Sleep   func(d time.Duration)
}

func (opts Options) Fn(consumerMap consumermap.Map) func(m *kafka.Message) (commitNow bool, err error) {
	if opts.RetryDelay == 0 || opts.RetryDuration == 0 {
		panic("empty RetryDelay or empty RetryDuration")
	}
	if opts.Produce == nil {
		panic("empty Produce func")
	}
	if opts.Now == nil {
		opts.Now = time.Now
	}
	if opts.Sleep == nil {
		opts.Sleep = time.Sleep
	}

	return func(m *kafka.Message) (bool, error) {
		topic := *m.TopicPartition.Topic
		if strings.HasSuffix(topic, extFailed) {
			return false, nil // do not consume failed message
		}

		var topicRetry, topicFailed string
		switch {

		case strings.HasSuffix(topic, extRetry):
			topicRetry = topic
			topicFailed = topic[:len(topic)-len(extRetry)] + extFailed

		default:
			topicRetry = topic + extRetry
			topicFailed = topic + extFailed

		}

		produceFailedMessage := func(headers []kafka.Header, err error, errCount int) error {
			headers = appendStringToHeader(HeaderError, err.Error(), headers)
			if errCount != 0 {
				headers = appendIntToHeader(HeaderErrorCount, errCount, headers)
			}
			return opts.Produce(topicFailed, string(m.Key), m.Value, headers)
		}

		produceRetryMessage := func(headers []kafka.Header, err error, errCount int, runAfter, runBefore time.Time) error {
			headers = appendStringToHeader(HeaderError, err.Error(), headers)
			headers = appendIntToHeader(HeaderErrorCount, errCount, headers)
			headers = appendTimeToHeader(HeaderRunAfter, runAfter, headers)
			headers = appendTimeToHeader(HeaderRunBefore, runBefore, headers)
			return opts.Produce(topicRetry, string(m.Key), m.Value, headers)
		}

		errMsg, errCount, runAfter, runBefore, headers := scanHeaders(m.Headers)

		f, ok := consumerMap[topic]
		if !ok {
			return false, produceFailedMessage(headers, errTopicHasNoConsumer(topic), errCount)
		}

		now := opts.Now()
		if runAfter != zeroTime && now.Before(runAfter) {
			opts.Sleep(runAfter.Sub(now))
		}

		now = opts.Now()
		if runBefore != zeroTime && now.After(runBefore) {
			return false, produceFailedMessage(headers, errors.New(errMsg), errCount)
		}

		// Handle the message
		err := f(m)
		if err != nil {
			errCount++

			if kafkaerr.IsDoNotRetry(err) {
				return false, produceFailedMessage(headers, err, errCount)
			}

			now = opts.Now()
			runAfter = now.Add(opts.RetryDelay)
			if runBefore == zeroTime {
				runBefore = now.Add(opts.RetryDuration)
			}

			if runAfter.Before(runBefore) {
				return false, produceRetryMessage(headers, err, errCount, runAfter, runBefore)
			} else {
				return false, produceFailedMessage(headers, err, errCount)
			}
		}

		return false, nil
	}
}
