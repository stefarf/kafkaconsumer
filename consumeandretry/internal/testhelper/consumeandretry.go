package testhelper

import (
	"fmt"
	"reflect"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"

	"gitlab.com/stefarf/kafkaconsumer/consumeandretry"
)

func ConsumeRetryDefaultOptions(retryDelay, retryDuration time.Duration) consumeandretry.Options {
	return consumeandretry.Options{
		RetryDelay:    retryDelay,
		RetryDuration: retryDuration,
		Produce: func(topic, key string, value any, headers []kafka.Header) error {
			panic(fmt.Sprintf("unexpected call produce(topic='%s', key='%s', value='%s', headers='%v')",
				topic, key, value, headers))
		},
	}
}

func ProduceFnMustBeCalledWithArgsAndReturn(topicIn, keyIn string, valueIn any, headersIn []kafka.Header, errReturned error) func(topic, key string, value any, headers []kafka.Header) error {
	return func(topic, key string, value any, headers []kafka.Header) error {
		match := (topicIn == topic) && (keyIn == key) && reflect.DeepEqual(valueIn, value) && reflect.DeepEqual(headersIn, headers)
		if !match {
			panic(fmt.Sprintf(
				"Unmatched call:\nACTUAL call: produce(topic='%s', key='%s', value='%s', headers='%s')\nEXPECTED call: produce(topic='%s', key='%s', value='%s', headers='%s')",
				topic, key, value, headers,
				topicIn, keyIn, valueIn, headersIn))
		}
		return errReturned
	}
}
