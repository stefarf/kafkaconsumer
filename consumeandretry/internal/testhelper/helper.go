package testhelper

import (
	"math"
	"testing"
	"time"
)

func MustSleepSecs(t *testing.T, want float64) func(d time.Duration) {
	return func(d time.Duration) {
		actual := d.Seconds()
		if math.Abs(actual-want) > 1e-6 {
			t.Errorf("want sleep %.1f secs, actual %.1f secs", want, actual)
		}
	}
}

func PtrString(s string) *string { return &s }
