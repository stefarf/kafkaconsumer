package consumeandretry

import (
	"strconv"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

const (
	HeaderError      = "error"
	HeaderErrorCount = "error_count"
	HeaderRunAfter   = "run_after"
	HeaderRunBefore  = "run_before"
)

func appendStringToHeader(key, val string, headers []kafka.Header) []kafka.Header {
	return append(headers, kafka.Header{Key: key, Value: []byte(val)})
}

func appendIntToHeader(key string, val int, headers []kafka.Header) []kafka.Header {
	return append(headers, kafka.Header{Key: key, Value: []byte(strconv.FormatInt(int64(val), 10))})
}

func appendTimeToHeader(key string, val time.Time, headers []kafka.Header) []kafka.Header {
	return append(headers, kafka.Header{Key: key, Value: []byte(val.Format(time.RFC3339))})
}

func scanHeaders(headers []kafka.Header) (errMsg string, errCount int, runAfter, runBefore time.Time, remaining []kafka.Header) {
	for _, h := range headers {
		switch h.Key {
		case HeaderError:
			errMsg = string(h.Value)
		case HeaderErrorCount:
			i, _ := strconv.ParseInt(string(h.Value), 10, 64)
			errCount = int(i)
		case HeaderRunAfter:
			t, err := time.Parse(time.RFC3339, string(h.Value))
			if err == nil {
				runAfter = t
			}
		case HeaderRunBefore:
			t, err := time.Parse(time.RFC3339, string(h.Value))
			if err == nil {
				runBefore = t
			}
		default:
			remaining = append(remaining, h)
		}
	}
	return
}
