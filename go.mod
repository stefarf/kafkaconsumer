module gitlab.com/stefarf/kafkaconsumer

go 1.20

require (
	github.com/confluentinc/confluent-kafka-go/v2 v2.2.0
	github.com/stretchr/testify v1.8.2
	gitlab.com/stefarf/iferr v0.1.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
