package consumermap

import "github.com/confluentinc/confluent-kafka-go/v2/kafka"

type Map map[Topic]Consumer
type Topic = string
type Consumer func(m *kafka.Message) error

func New() Map { return Map{} }

func (m Map) Register(topic string, consumer Consumer) Map {
	m[topic] = consumer
	return m
}

func (m Map) RegisterWithRetry(topic string, consumer Consumer) Map {
	m[topic] = consumer
	m[topic+".retry"] = consumer
	return m
}

func (m Map) Topics() []string {
	var topics []string
	for topicName := range m {
		topics = append(topics, topicName)
	}
	return topics
}
